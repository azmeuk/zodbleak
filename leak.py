import os
import psutil
import transaction
import sys
import ZODB
from ZODB.MappingStorage import MappingStorage
from BTrees.OOBTree import OOBTree

OUTPUT = "{}.data".format(os.path.basename(os.environ["VIRTUAL_ENV"]))
NB_READS = 10000
exit_code = 0

process = psutil.Process(os.getpid())


conn = ZODB.DB(MappingStorage()).open()
conn.root()["foobar"] = OOBTree()
transaction.commit()

before = process.memory_full_info().rss
with open(OUTPUT, "w") as fd:
    for i in range(NB_READS):
        conn.root()["foobar"]
        conn.cacheMinimize()

        after = process.memory_full_info().rss
        fd.write("{} {}\n".format(i, after))
        if i > 1 and before != after:
            exit_code = 1
            print("Memory increased by %s kB to reach %s kB at iteration %s" % ((after - before) // 1024, after // 1024, i))
        before = after


conn.close()
sys.exit(exit_code)
