import transaction
import ZODB
from ZODB.MappingStorage import MappingStorage
from BTrees.OOBTree import OOBTree

NB_READS = 10000000

conn = ZODB.DB(MappingStorage()).open()
conn.root()["foobar"] = OOBTree()
transaction.commit()

for i in range(NB_READS):
    conn.root()["foobar"]
    conn.cacheMinimize()

conn.close()
