Testcase to show that a memory leak exists with ZODB under certain conditions when using `cacheMinimize`.

# Install

You will need `gnuplot` to generate the result graph.

```bash
virtualenv env
env/bin/pip install tox
env/bin/tox
cat plot.conf | gnuplot
open leak.png
```

**py27-zodb5** always fails, but I have already seen the following environment fail sometimes:

- py35-zodb4
- py36-zodb5


# Result

Result may differ depending on the environment it has been executed. Generally it looks like this:
![Result](https://raw.githubusercontent.com/azmeuk/zodbleak/master/results/eloi-archlinux.png)

To see the memory growing live you can run the following command:
```
virtualenv -p python2 env
env/bin/pip install zodb
env/bin/python poc.py
# Then, watch the memory consumption grow indefinitely with tools like htop.
```
